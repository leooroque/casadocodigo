package br.com.casadocodigo.loja.model;

import java.math.BigDecimal;

public class Precos {

	private BigDecimal valor;
	private TipoPrecos tipos;
	
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public TipoPrecos getTipos() {
		return tipos;
	}
	public void setTipos(TipoPrecos tipos) {
		this.tipos = tipos;
	}
	
}
