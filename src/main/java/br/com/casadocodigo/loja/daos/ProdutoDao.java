package br.com.casadocodigo.loja.daos;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casadocodigo.loja.model.Produto;

@Repository
public class ProdutoDao {
	
	@PersistenceContext
	private EntityManager manager ;
	
	
	@Transactional
	public void gravaProduto(Produto produto) {
		manager.persist(produto);
	}
	
	@Transactional
	public List<Produto> consultaProdutos(){

		return manager.createQuery("select p from Produto p",Produto.class).getResultList();
		
		
	}

}
