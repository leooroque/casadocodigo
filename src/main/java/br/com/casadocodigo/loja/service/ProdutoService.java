package br.com.casadocodigo.loja.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import br.com.casadocodigo.loja.daos.ProdutoDao;
import br.com.casadocodigo.loja.model.Produto;

@Component
public class ProdutoService {
	
	@Autowired
	private ProdutoDao dao;
	
	
	public ModelAndView getProdutos(String pagina) {
		ModelAndView modelView = new ModelAndView(pagina);
		List<Produto> produtos = dao.consultaProdutos();
		modelView.addObject("produtos", produtos);
		
		return modelView;
	}

}
