package br.com.casadocodigo.loja.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casadocodigo.loja.daos.ProdutoDao;
import br.com.casadocodigo.loja.model.Produto;
import br.com.casadocodigo.loja.service.ProdutoService;

@Controller
public class CadastroController {

	@Autowired
	private ProdutoDao dao;
	
	@Autowired
	ProdutoService serviceProduto;
	
	@RequestMapping("produto/form")
	public ModelAndView form(){
		ModelAndView modelView = new ModelAndView("produto/form");
		return modelView;
	}
	
	@RequestMapping(value="/produto", method=RequestMethod.POST)
	public ModelAndView cadastrar(Produto produto, RedirectAttributes redirectAttributes) {
		dao.gravaProduto(produto);
		redirectAttributes.addFlashAttribute("sucesso","Produto cadastrado com sucesso!");
		return new ModelAndView("redirect:/produto");
	}
	
	@RequestMapping(value="/produto", method=RequestMethod.GET)
	public ModelAndView consultar() {

		return serviceProduto.getProdutos("produto/listaProdutos");
		//ModelAndView modelView = new ModelAndView("produto/listaProdutos");


	
		//return modelView;
	}
}
